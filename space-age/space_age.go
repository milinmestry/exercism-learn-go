package space

type Planet string
type SpacePlanets struct {
	Name       Planet
	EarthYears float64
}

const (
	EarthYearsToSeconds = 31557600
	InvalidEarthAge     = -1.000000
)

// Define planets with its Earth year
var Planets map[Planet]SpacePlanets = map[Planet]SpacePlanets{
	"Earth":   {Name: "Earth", EarthYears: 1.0},
	"Jupiter": {Name: "Jupiter", EarthYears: 11.862615},
	"Mars":    {Name: "Mars", EarthYears: 1.8808158},
	"Mercury": {Name: "Mercury", EarthYears: 0.2408467},
	"Neptune": {Name: "Neptune", EarthYears: 164.79132},
	"Saturn":  {Name: "Saturn", EarthYears: 29.447498},
	"Venus":   {Name: "Venus", EarthYears: 0.61519726},
	"Uranus":  {Name: "Venus", EarthYears: 84.016846},
}

// Get number of seconds for given Planet earth year
func (spacePlanet *SpacePlanets) getSecondsForEarthYears() float64 {
	return (float64(EarthYearsToSeconds) * spacePlanet.EarthYears)
}

// Given an age in seconds, calculate how old someone would be on Planet
func Age(seconds float64, planet Planet) float64 {
	spacePlanet, isHabitable := Planets[planet]
	if !isHabitable {
		// Ohh! it's not planet, how can human servive here
		return InvalidEarthAge
	} else {
		return seconds / spacePlanet.getSecondsForEarthYears()
	}
}
