package sorting

import (
	"fmt"
	"strconv"
)

// DescribeNumber should return a string describing the number.
func DescribeNumber(f float64) string {
	return "This is the number " + strconv.FormatFloat(f, 'f', 1, 64)
	// panic("Please implement DescribeNumber")
}

type NumberBox interface {
	Number() int
}

// DescribeNumberBox should return a string describing the NumberBox.
func DescribeNumberBox(nb NumberBox) string {
	return fmt.Sprintf("This is a box containing the number %.1f", float64(nb.Number()))
	// panic("Please implement DescribeNumberBox")
}

type FancyNumber struct {
	n string
}

func (i FancyNumber) Value() string {
	return i.n
}

type FancyNumberBox interface {
	Value() string
}

// ExtractFancyNumber should return the integer value for a FancyNumber
// and 0 if any other FancyNumberBox is supplied.
func ExtractFancyNumber(fnb FancyNumberBox) int {
	val := "0"
	switch t := fnb.(type) {
	case FancyNumber:
		val = t.Value()
	}

	var i, _ = strconv.Atoi(val)
	return i
	// panic("Please implement ExtractFancyNumber")
}

// DescribeFancyNumberBox should return a string describing the FancyNumberBox.
func DescribeFancyNumberBox(fnb FancyNumberBox) string {
	return fmt.Sprintf("This is a fancy box containing the number %.1f", float64(ExtractFancyNumber(fnb)))
	// panic("Please implement DescribeFancyNumberBox")
}

// DescribeAnything should return a string describing whatever it contains.
func DescribeAnything(i interface{}) string {
	msg := "Return to sender"
	switch val := i.(type) {
	case int:
		msg = DescribeNumber(float64(val))
	case float64:
		msg = DescribeNumber(val)
	case NumberBox:
		msg = DescribeNumberBox(val)
	case FancyNumberBox:
		msg = DescribeFancyNumberBox(val)
	}
	return msg
	// panic("Please implement DescribeAnything")
}
