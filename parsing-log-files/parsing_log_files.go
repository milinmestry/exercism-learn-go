package parsinglogfiles

import (
	"regexp"
)

func IsValidLine(text string) bool {
	reg, err := regexp.Compile(`^\[(TRC|DBG|INF|WRN|ERR|FTL)\]`)
	if err != nil {
		panic("Error in regex")
	}
	return reg.MatchString(text)
}

func SplitLogLine(text string) (result []string) {
	reg, err := regexp.Compile(`(<[^A-Za-z&]*>)`)
	if err != nil {
		panic("Error in regex")
	}
	result = reg.Split(text, -1)
	return
	// panic("Please implement the SplitLogLine function")
}

func CountQuotedPasswords(lines []string) (numCount int) {
	reg, err := regexp.Compile(`(?i)".*password.*"`)
	if err != nil {
		panic("Error in regex")
	}
	for _, text := range lines {
		if reg.MatchString(text) {
			numCount += 1
		}
	}
	return
	// panic("Please implement the CountQuotedPasswords function")
}

func RemoveEndOfLineText(text string) (newText string) {
	reg, err := regexp.Compile(`end-of-line[0-9]+`)
	if err != nil {
		panic("Error in regex")
	}
	newText = reg.ReplaceAllString(text, "")
	return
	// panic("Please implement the RemoveEndOfLineText function")
}

func TagWithUserName(lines []string) (tags []string) {
	reg, err := regexp.Compile(`User\s+(\w+)`)
	if err != nil {
		panic("Error in regex")
	}

	for _, text := range lines {
		userWithName := reg.FindStringSubmatch(text)
		if len(userWithName) > 1 {
			text = "[USR] " + userWithName[1] + " " + text
		}
		tags = append(tags, text)
	}
	return
	// panic("Please implement the TagWithUserName function")
}
