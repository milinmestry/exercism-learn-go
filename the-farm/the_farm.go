package thefarm

import (
	"errors"
	"fmt"
)

// See types.go for the types defined for this exercise.

// SillyNephewError defined error message using Error() method.
type SillyNephewError struct {
	message string
	// details string
}

func (e *SillyNephewError) Error() string {
	return fmt.Sprintf("silly nephew, there cannot be %s", e.message)
}

// sillyNephewNegativeCows return error when provided cows number is negative.
func sillyNephewNegativeCows(cows int) error {
	// ...
	return &SillyNephewError{
		message: fmt.Sprintf("%d cows", cows),
	}
}

var (
	// Error message, Division by 0
	ErrDivisionByZero = errors.New("division by zero")
	// Error message, negative fodder
	ErrNegativeFodder = errors.New("negative fodder")
)

// DivideFood computes the fodder amount per cow for the given cows.
func DivideFood(weightFodder WeightFodder, cows int) (float64, error) {
	// Handle silly nephew error
	if cows < 0 {
		return 0.0, sillyNephewNegativeCows(cows)
	}

	// Handle division by 0 error
	if cows == 0 {
		return 0.0, ErrDivisionByZero
	}

	// Read the fodder amount
	amount, err := weightFodder.FodderAmount()

	// Handle the errors
	if err != nil {
		// Scale Malfunction
		if err == ErrScaleMalfunction {
			// Negative fodder
			if amount < 0 {
				return 0.0, ErrNegativeFodder
			} else {
				// double the fodder amount
				amount *= 2
			}
		} else {
			// return all other errors
			return 0.0, err
		}
	}

	// Negative fodder
	if amount < 0 {
		return 0.0, ErrNegativeFodder
	}

	return amount / float64(cows), nil
	// panic("Please implement DivideFood")
}
