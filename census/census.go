// Package census simulates a system used to collect census data.
package census

// Resident represents a resident in this city.
type Resident struct {
	Address map[string]string
	Age     int
	Name    string
}

// NewResident registers a new resident in this city.
func NewResident(name string, age int, address map[string]string) *Resident {
	return &Resident{Name: name, Age: age, Address: address}
	// panic("Please implement NewResident.")
}

// HasRequiredInfo determines if a given resident has all of the required information.
func (r *Resident) HasRequiredInfo() bool {
	return r.Name != "" && r.Address["street"] != ""

	// if r.Name != "" && r.Address["street"] != "" {
	// 	return true
	// }
	// return false

	// panic("Please implement HasRequiredInfo.")
}

// Delete deletes a resident's information.
func (r *Resident) Delete() {
	*r = Resident{}
	// r.Address = nil
	// r.Age = 0
	// r.Name = ""
	// panic("Please implement Delete.")
}

// Count counts all residents that have provided the required information.
func Count(residents []*Resident) int {
	totalResidents := 0
	for _, r := range residents {
		if r.HasRequiredInfo() {
			totalResidents++
		}
	}
	return totalResidents
	// panic("Please implement Count.")
}
