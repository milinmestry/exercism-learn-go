package dna

import (
	"errors"
)

// Histogram is a mapping from nucleotide to its count in given DNA.
// Choose a suitable data type.
type Histogram map[rune]int

// DNA is a list of nucleotides. Choose a suitable data type.
type DNA string

// Error string for invalid nucleotides
var ErrInvalidNucleotides = errors.New("strand with invalid nucleotides")

// Counts generates a histogram of valid nucleotides in the given DNA.
// Returns an error if d contains an invalid nucleotide.
//
// Counts is a method on the DNA type. A method is a function with a special receiver argument.
// The receiver appears in its own argument list between the func keyword and the method name.
// Here, the Counts method has a receiver of type DNA named d.
func (d DNA) Counts() (Histogram, error) {
	var h Histogram = Histogram{'A': 0, 'C': 0, 'G': 0, 'T': 0}

	// loop through each nucleotide
	for _, nucleotide := range d {
		// nucleotide not in a bit of DNA a "DNA sequence", raise error
		if _, ok := h[nucleotide]; !ok {
			return h, ErrInvalidNucleotides
		} else {
			// sum each nucleotide that is present as per Histogram
			h[nucleotide]++
		}
	}
	return h, nil
}
