package strand

// RNA complement of a given DNA sequence
type Histogram map[rune]string

// Given a DNA strand, its transcribed RNA strand is formed by replacing each nucleotide with its complement
func ToRNA(dna string) string {
	var rnaToDna Histogram = Histogram{'A': "U", 'C': "G", 'G': "C", 'T': "A"}
	strRNA := ""

	// Loop through each DNA nucleotide
	for _, nucleotide := range dna {
		if rna, present := rnaToDna[nucleotide]; present {
			strRNA += rna
		}
	}
	return strRNA
}
