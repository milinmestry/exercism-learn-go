package logs

import (
	"strings"
	"unicode/utf8"
)

var AppLogTypes map[rune]string = map[rune]string{
	'\u2757':     "recommendation",
	'\U0001f50d': "search",
	'\u2600':     "weather",
}

const appDefault string = "default"

// Application identifies the application emitting the given log.
func Application(log string) string {
	for _, r := range log {
		if application, ok := AppLogTypes[r]; ok {
			return application
		}
	}
	return appDefault
	// panic("Please implement the Application() function")
}

// Replace replaces all occurrences of old with new, returning the modified log
// to the caller.
func Replace(log string, oldRune, newRune rune) string {
	return strings.ReplaceAll(log, string(oldRune), string(newRune))
	// panic("Please implement the Replace() function")
}

// WithinLimit determines whether or not the number of characters in log is
// within the limit.
func WithinLimit(log string, limit int) bool {
	return utf8.RuneCountInString(log) <= limit
	// panic("Please implement the WithinLimit() function")
}
