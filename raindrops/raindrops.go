package raindrops

import (
	"strconv"
)

// convert a number into a string that contains raindrop sounds
// corresponding to certain potential factors
func Convert(number int) (result string) {
	if number%3 == 0 {
		result = "Pling"
	}
	if number%5 == 0 {
		result += "Plang"
	}
	if number%7 == 0 {
		result += "Plong"
	}
	if result == "" {
		result = strconv.Itoa(number)
	}

	return
}
