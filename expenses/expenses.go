package expenses

import (
	"fmt"
)

// Record represents an expense record.
type Record struct {
	Day      int
	Amount   float64
	Category string
}

// DaysPeriod represents a period of days for expenses.
type DaysPeriod struct {
	From int
	To   int
}

// UnknownCategoryError defined error message using Error() method.
type UnknownCategoryError struct {
	category string
}

// Error return unknown category string message
func (e *UnknownCategoryError) Error() string {
	return fmt.Sprintf("unknown category %s", e.category)
}

// Filter returns the records for which the predicate function returns true.
func Filter(in []Record, predicate func(Record) bool) (result []Record) {
	for _, r := range in {
		if predicate(r) {
			result = append(result, r)
		}
	}
	return
	// panic("Please implement the Filter function")
}

// ByDaysPeriod returns predicate function that returns true when
// the day of the record is inside the period of day and false otherwise
func ByDaysPeriod(p DaysPeriod) func(Record) bool {
	return func(r Record) bool {
		return p.From <= r.Day && r.Day <= p.To
	}
	// panic("Please implement the ByDaysPeriod function")
}

// ByCategory returns predicate function that returns true when
// the category of the record is the same as the provided category
// and false otherwise
func ByCategory(c string) func(Record) bool {
	return func(r Record) bool {
		return c == r.Category
	}
	// panic("Please implement the ByCategory function")
}

// TotalByPeriod returns total amount of expenses for records
// inside the period p
func TotalByPeriod(in []Record, p DaysPeriod) (totalAmount float64) {
	for _, r := range Filter(in, ByDaysPeriod(p)) {
		totalAmount += r.Amount
	}
	return
	// panic("Please implement the TotalByPeriod function")
}

// CategoryExpenses returns total amount of expenses for records
// in category c that are also inside the period p.
// An error must be returned only if there are no records in the list that belong
// to the given category, regardless of period of time.
func CategoryExpenses(in []Record, p DaysPeriod, c string) (float64, error) {
	recordsByCategory := Filter(in, ByCategory(c))
	if len(recordsByCategory) == 0 {
		return 0, &UnknownCategoryError{category: c}
	}
	return TotalByPeriod(recordsByCategory, p), nil
	// panic("Please implement the CategoryExpenses function")
}
