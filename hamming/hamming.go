package hamming

import "errors"

// string length mismatch error
var ErrorDnaLength = errors.New("provided DNA length does not match")

// Distance returns the hamming distance of two strings.
// The comparison is done at the character level
func Distance(a, b string) (int, error) {
	diffCount := 0

	// Mismatch length, return error
	if len(a) != len(b) {
		return diffCount, ErrorDnaLength
	}
	// Count the difference between both the string
	for i := range a {
		if a[i] != b[i] {
			diffCount++
		}
	}
	return diffCount, nil
}
